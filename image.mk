ifndef CI_REGISTRY
CI_REGISTRY := registry.icinga.com
endif
ifeq ($(CI_PROJECT_PATH),)
CI_PROJECT_PATH := build-docker/ubuntu
endif

FROM := $(shell grep FROM Dockerfile | cut -d" " -f2)
DIST := $(shell basename $$(dirname `pwd`))
VARIANT := $(shell basename `pwd`)

IMAGE := $(CI_PROJECT_PATH)/$(DIST)

ifneq ($(CI_REGISTRY),)
IMAGE := $(CI_REGISTRY)/$(IMAGE)
endif

ifeq ($(VARIANT),x86_64)
IMAGE_EXTRA_TAG := latest
endif

all: pull build

pull:
	docker pull "$(FROM)"

build:
	docker build --tag "$(IMAGE):$(VARIANT)" .
ifdef IMAGE_EXTRA_TAG
	docker tag "$(IMAGE):$(VARIANT)" "$(IMAGE):$(IMAGE_EXTRA_TAG)"
endif

push:
	docker push "$(IMAGE):$(VARIANT)"
ifdef IMAGE_EXTRA_TAG
	docker push "$(IMAGE):$(IMAGE_EXTRA_TAG)"
endif

clean:
	if (docker inspect --type image "$(IMAGE)" >/dev/null 2>&1); then docker rmi "$(IMAGE)"; fi
